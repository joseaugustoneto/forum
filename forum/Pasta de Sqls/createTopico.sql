CREATE TABLE admin.topico
(
  ID_Topic int NOT NULL,
  Titulo Char(150) NOT NULL,
  Data_Criacao Char(150) NOT NULL,
  ID_User int NOT NULL,
  Desc_Topic Char(250) NOT NULL,
 PRIMARY KEY (ID_Topic, ID_User)
);