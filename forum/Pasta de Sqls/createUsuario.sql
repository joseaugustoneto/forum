DROP TABLE ADMIN.USUARIO;
CREATE TABLE "ADMIN".usuario
(
ID_user INT not null GENERATED ALWAYS AS IDENTITY,
  Nome Char(150) NOT NULL,
  Data_Nascimento Char(10) NOT NULL,
  UF Char(10),
  Cidade Char(50),
  Email Char(200) NOT NULL,
  Sexo Char(1) NOT NULL,
  Senha Char(20) NOT NULL,
   PRIMARY KEY (ID_user)

);