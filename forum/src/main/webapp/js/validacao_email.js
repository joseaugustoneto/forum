function checar() {
	var valor_email = $("#email").val();
	var arroba = false;
	for (var i = 0; i < valor_email.length; i++) {
		if (valor_email.substring(i, i + 1) == "@") {
			arroba = true;
		}
	}
	if (arroba != true) {
		nao_valido("#email");
	} else {
		valido("#email");
	}
}

function checar_senha() {
	var valor_senha = $("#senha").val();

	if (valor_senha.length >= 3) {
		valido("#senha");
	}
	else{
		nao_valido("#senha");
	}
}

function valido(id){
	$(id).addClass("verde");
	$(id).removeClass("vermelho");
}

function nao_valido(id){
	$(id).addClass("vermelho");
	$(id).removeClass("verde");
}