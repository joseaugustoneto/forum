package forum.Model;

import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

@ManagedBean
@ApplicationScoped
public class UsuarioService {

	//@ManagedProperty(value="#{usuarioDao}")
	private UsuarioDao usuarioDao = new UsuarioDao();

	public UsuarioDao getUsuarioDao() {
		return usuarioDao;
	}

	public void setUsuarioDao(UsuarioDao usuarioDao) {
		this.usuarioDao = usuarioDao;
	}

	public void salvar(Usuario usuario) {
		usuarioDao.incluirUsuario(usuario);
	}

	public List<Usuario> obterUsuarios() {
		System.out.println("entrou no obterUsuarios");
		return usuarioDao.obterUsuario();
	}

	public void excluir(Usuario usuario) {
		usuarioDao.excluirUsuario(usuario);
	}
	
	public Usuario logar(Usuario usuario){
		System.out.println("entrou no metodo logar "+usuario.getEmail());
		List<Usuario> listUsers;
		System.out.println("criou o list de usuarios");
		listUsers = obterUsuarios();
		System.out.println("obteve os usuarios do banco.");
		Usuario encontrado = new Usuario();
		System.out.println("criou o usuario para receber o encontrado");
		for(int i=0; i<= listUsers.size(); i++){
			System.out.println("entrou no for");
			Usuario u = (Usuario) listUsers.get(i);
			System.out.println(u.getCidade()+" u u");
			System.out.println("criou o usuario u e recebeu um usuario do array (usuarioservice)");
			if(usuario.getEmail().equalsIgnoreCase(u.getEmail())){
				System.out.println("entrou no primeiro if para localizar usuario (usuarioservice)");
				if(usuario.getSenha().equals(u.getSenha())){
					encontrado = u;
					System.out.println(u.getCidade()+" :)");
					System.out.println("encontrado recebeu u");
					break;
				}
			}
			System.out.println("nao encontrou o usuario");
		}
		System.out.println("saiu do for");
		listUsers.clear();
		return encontrado;
	}
}
