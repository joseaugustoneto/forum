package forum.Model;

import java.sql.Timestamp;
import java.util.Date;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean
@ApplicationScoped
public class Comentario {
	
	
	
	private String nome_usuario;
	private String titulo_comentario;
	private String descricao;
	private String data_comentario;
	private String data_topico;
	private int id_coment;
	private String desc_topic;
	private int id_topic;
	private int id_user;
	
	ComentarioService comentarioService = new ComentarioService();
	
	public int getId_user() {
		return id_user;
	}
	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	public int getId_topic() {
		return id_topic;
	}
	public void setId_topic(int id_topic) {
		this.id_topic = id_topic;
	}
	public String getNome_usuario() {
		return nome_usuario;
	}
	public void setNome_usuario(String nome_usuario) {
		this.nome_usuario = nome_usuario;
	}
	public String getTitulo_comentario() {
		return titulo_comentario;
	}
	public void setTitulo_comentario(String titulo_comentario) {
		this.titulo_comentario = titulo_comentario;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getData_comentario() {
		return data_comentario;
	}
	public void setData_comentario(String data_comentario) {
		this.data_comentario = data_comentario;
	}
	public String getData_topico() {
		return data_topico;
	}
	public void setData_topico(String data_topico) {
		this.data_topico = data_topico;
	}
	public int getId_coment() {
		return id_coment;
	}
	public void setId_coment(int id_coment) {
		this.id_coment = id_coment;
	}
	public String getDesc_topic() {
		return desc_topic;
	}
	public void setDesc_topic(String desc_topic) {
		this.desc_topic = desc_topic;
	}
	
	public String agora() {
		Timestamp dataTimestamp = new Timestamp(new Date().getTime());
		return dataTimestamp.toString();
	}


}
