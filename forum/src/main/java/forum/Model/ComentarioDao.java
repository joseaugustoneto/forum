package forum.Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
//import java.sql.Timestamp;
import java.util.ArrayList;
//import java.util.Date;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean
@ApplicationScoped
public class ComentarioDao {

	public ComentarioDao() {
		try {
			// Carrega o driver do banco de dados.
			Class.forName("org.apache.derby.jdbc.ClientDriver");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public List<Comentario> obterComentario(int topico) {
		// Cria uma lista de topicos
		List<Comentario> Comentarios = new ArrayList<Comentario>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = JdbcUtil.createConnection();
			// Obtém uma sentença SQL.
			stmt = conn.createStatement();
			// Executa a instrução SQL.
			rs = stmt
					.executeQuery("select C.ID_topic, C.descricao, C.data_criacao as data_comentario, C.id_coment, U.nome, T.titulo, t.data_criacao as data_topico  from COMENTARIO C, USUARIO U, TOPICO T where C.ID_topic = " + topico);
			// Percorre todos os registros.
			while (rs.next()) {
				String descricao = rs.getString("descricao");
				String data_comentario = rs.getString("data_comentario");
				int id_coment = rs.getInt("id_coment");
				String nome_usuario = rs.getString("nome");
				String titulo_comentario = rs.getString("titulo");
				String data_topico = rs.getString("data_topico");
				String desc_topic = rs.getString("desc_topic");
				int id_topic = rs.getInt("id_topic");

				Comentario comentario = new Comentario();
				comentario.setDescricao(descricao);
				comentario.setId_coment(id_coment);
				comentario.setData_comentario(data_comentario);
				comentario.setNome_usuario(nome_usuario);
				comentario.setTitulo_comentario(titulo_comentario);
				comentario.setData_topico(data_topico);
				comentario.setDesc_topic(desc_topic);
				comentario.setId_topic(id_topic);
				Comentarios.add(comentario);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtil.close(conn, stmt, rs);
		}
		return Comentarios;
	}
	
	public int obterCod() {
		// Cria uma lista de topicos
		int codigo = 0;
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = JdbcUtil.createConnection();
			// Obtém uma sentença SQL.
			stmt = conn.createStatement();
			// Executa a instrução SQL.
			rs = stmt
					.executeQuery("select cod from cod_topico");
			// Percorre todos os registros.
			while (rs.next()) {
				int cod = rs.getInt("cod");
				
				codigo = cod;
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtil.close(conn, stmt, rs);
		}
		return codigo;
	}

	public void incluirComentario(Comentario comentario) {
		Connection conn = null;
		Statement stmt = null;
		try {
			conn = JdbcUtil.createConnection();
			// Inicia uma transação.
			conn.setAutoCommit(false);
			// Obtém uma sentença SQL.
			stmt = conn.createStatement();
			// Executa a instrução SQL.
			// String sql =
			// "insert into topico (id_user, id_topic, titulo, data_criacao, Desc_Topic) values('1', '1', 'sodifj',null,'dsoifjdfi')";
			// stmt.executeUpdate(sql);

			PreparedStatement pstmt = conn
					.prepareStatement("insert into comentario "
							+ "(id_topic, data_criacao, id_user, descricao) "
							+ "values " + "(?, ?, ?, ?)");
			pstmt.setInt(1, comentario.getId_topic());
			pstmt.setString(3, comentario.agora());
			pstmt.setInt(2, comentario.getId_user());
			pstmt.setString(4, comentario.getDescricao());
			pstmt.executeUpdate();
			
			conn.commit();

		} catch (Exception e) {

			try {
				if (conn != null && !conn.isClosed()) {
					// Cancela transação.
					conn.rollback();
				}
			} catch (Exception e1) {
				// Não faz nada.
			}

			throw new RuntimeException(e);
		} finally {
			JdbcUtil.close(conn, stmt);
		}
	}
	
	public void incluirCod(int cod){
		Connection conn = null;
		Statement stmt = null;
		try {
			conn = JdbcUtil.createConnection();
			// Inicia uma transação.
			conn.setAutoCommit(false);
			// Obtém uma sentença SQL.
			stmt = conn.createStatement();
			// Executa a instrução SQL.
			// String sql =
			// "insert into topico (id_user, id_topic, titulo, data_criacao, Desc_Topic) values('1', '1', 'sodifj',null,'dsoifjdfi')";
			// stmt.executeUpdate(sql);

			PreparedStatement pstmt = conn
					.prepareStatement("insert into cod_topico "
							+ "(cod) "
							+ "values " + "(?)");
			pstmt.setInt(1, cod);
			pstmt.executeUpdate();
			
			conn.commit();

		} catch (Exception e) {

			try {
				if (conn != null && !conn.isClosed()) {
					// Cancela transação.
					conn.rollback();
				}
			} catch (Exception e1) {
				// Não faz nada.
			}

			throw new RuntimeException(e);
		} finally {
			JdbcUtil.close(conn, stmt);
		}
	}

	public void excluirComentario(Comentario comentario) {
		Connection conn = null;
		Statement stmt = null;
		try {
			conn = JdbcUtil.createConnection();
			// Obtém uma sentença SQL.
			stmt = conn.createStatement();
			// Executa a instrução SQL.
			stmt.executeUpdate("delete from comentario where topico.id = "
					+ comentario.getId_coment());
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtil.close(conn, stmt);
		}
	}
}
