package forum.Model;

import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

@ManagedBean
@ApplicationScoped
public class TopicoService {

	@ManagedProperty(value="#{topicoDao}")
	private TopicoDao topicoDao;

	public TopicoDao getTopicoDao() {
		return topicoDao;
	}

	public void setTopicoDao(TopicoDao topicoDao) {
		this.topicoDao = topicoDao;
	}

	public void salvar(Topico topico) {
		topicoDao.incluirTopico(topico);
	}

	public List<Topico> obterTopicos() {
		return topicoDao.obterTopico();
	}

	public void excluir(Topico topico) {
		topicoDao.excluirTopico(topico);
	}
}
