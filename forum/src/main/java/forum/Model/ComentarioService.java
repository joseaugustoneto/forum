package forum.Model;

import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

@ManagedBean
@ApplicationScoped
public class ComentarioService {
	
	ComentarioDao comentarioDao = new ComentarioDao();

	public ComentarioDao getTopicoDao() {
		return comentarioDao;
	}

	public void setTopicoDao(ComentarioDao comentarioDao) {
		this.comentarioDao = comentarioDao;
	}

	public void salvar(Comentario comentario) {
		comentarioDao.incluirComentario(comentario);
	}

	public List<Comentario> obterComentarios(int cod_topico) {
		return comentarioDao.obterComentario(cod_topico);
	}

	public void excluir(Comentario comentario) {
		comentarioDao.excluirComentario(comentario);
	}

	public void salvarCod(int id_topico) {
		comentarioDao.incluirCod(id_topico);
	}
	
	public int buscarCod() {
		return comentarioDao.obterCod();
	}
}
