package forum.Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
//import java.sql.Timestamp;
import java.util.ArrayList;
//import java.util.Date;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean
@ApplicationScoped
public class UsuarioDao {

	public UsuarioDao() {
		try {
			//Carrega o driver do banco de dados.
			Class.forName("org.apache.derby.jdbc.ClientDriver");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<Usuario> obterUsuario() {
		//Cria uma lista de topicos
		System.out.println("entrou no obter usuario do UsuarioDAO");
		List<Usuario> usuarios = new ArrayList<Usuario>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		System.out.println("Criou as conexoes com o banco");
		try {
			conn = JdbcUtil.createConnection();
			System.out.println("conn = JdbcUtil.createConnection();");
			//Obtém uma sentença SQL.
			stmt = conn.createStatement();
			System.out.println("stmt = conn.createStatement();");
			//Executa a instrução SQL.
			rs = stmt.executeQuery("select email, senha, nome, id_user, cidade, sexo, uf, data_nascimento "
					+ "from usuario");
			//Percorre todos os registros.
			System.out.println("dentro do try de obter usuarios do DAO");
			while (rs.next()) {
				String email = rs.getString("email");
				String senha = rs.getString("senha");
				String nome = rs.getString("nome");
				String id = rs.getString("id_user");
				String cidade = rs.getString("cidade");
				String sexo = rs.getString("sexo");
				String uf = rs.getString("uf");
				String data_nascimento = rs.getString("data_nascimento");
				int idAux = Integer.parseInt(id);
				
				Usuario usuario = new Usuario();
				usuario.setEmail(email);
				usuario.setSenha(senha);
				usuario.setNome(nome);
				usuario.setId_user(idAux);
				usuario.setCidade(cidade);
				usuario.setSexo(sexo);
				usuario.setUf(uf);
				usuario.setData_nascimento(data_nascimento);	
				usuarios.add(usuario);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtil.close(conn, stmt, rs);
		}
		System.out.println("vai retornar usuarios para UsuarioService");
		return usuarios;
	}

	public void incluirUsuario(Usuario usuario) {
		Connection conn = null;
		Statement stmt = null;
		try {
			conn = JdbcUtil.createConnection();
			//Inicia uma transação.
			conn.setAutoCommit(false);
			//Obtém uma sentença SQL.
			stmt = conn.createStatement();
			//Executa a instrução SQL.
//			String sql = "insert into topico (id_user, id_topic, titulo, data_criacao, Desc_Topic) values('1', '1', 'sodifj',null,'dsoifjdfi')";
//			stmt.executeUpdate(sql);
			
			PreparedStatement pstmt = conn.prepareStatement(
					"insert into usuario "
					+ "(senha, sexo, email, cidade, uf, data_nascimento, nome) "
					+ "values "
					+ "(?, ?, ?, ?, ?, ?, ?)");
			pstmt.setString(1, usuario.getSenha());
			pstmt.setString(2, usuario.getSexo());
			pstmt.setString(3, usuario.getEmail());
			pstmt.setString(4, usuario.getCidade());
			pstmt.setString(5, usuario.getUf());
			pstmt.setString(6, usuario.getData_nascimento());
			pstmt.setString(7, usuario.getNome());
			//pstmt.setInt(8, usuario.getId_user());
			pstmt.executeUpdate();

			//...
			//Isso é um crash!
			//if (topico.getId_topico().startsWith("1")) throw new RuntimeException("Yeh Yeh!");
			//...
			
//			PreparedStatement pstmt = conn.prepareStatement(
//					"insert into log_curso "
//					+ "(data_hora, operacao, codigo) "
//					+ "values "
//					+ "(?, ?, ?)");
//			pstmt.setTimestamp(1, agora());
//			pstmt.setString(2, "incluir");
//			pstmt.setString(3, topico.getId_topico());
//			pstmt.executeUpdate();

			//Efetivação da transação.
			conn.commit();
			
		} catch (Exception e) {

			try {
				if (conn != null && !conn.isClosed()) {
					//Cancela transação.
					conn.rollback();
				}
			} catch (Exception e1) {
				//Não faz nada.
			}

			throw new RuntimeException(e);
		} finally {
			JdbcUtil.close(conn, stmt);
		}
	}

//	private Timestamp agora() {
//		return new Timestamp(new Date().getTime());
//	}

	public void excluirUsuario(Usuario usuario) {
		Connection conn = null;
		Statement stmt = null;
		try {
			conn = JdbcUtil.createConnection();
			//Obtém uma sentença SQL.
			stmt = conn.createStatement();
			//Executa a instrução SQL.
			stmt.executeUpdate("delete from topico where topico.id = "
					+ usuario.getId_user());
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtil.close(conn, stmt);
		}
	}
}
