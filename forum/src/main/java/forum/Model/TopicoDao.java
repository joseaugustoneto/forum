package forum.Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
//import java.sql.Timestamp;
import java.util.ArrayList;
//import java.util.Date;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import forum.Controller.UsuarioLogadoController;

@ManagedBean
@ApplicationScoped
public class TopicoDao {

	public TopicoDao() {
		try {
			//Carrega o driver do banco de dados.
			Class.forName("org.apache.derby.jdbc.ClientDriver");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<Topico> obterTopico() {
		//Cria uma lista de topicos
		List<Topico> topicos = new ArrayList<Topico>();
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = JdbcUtil.createConnection();
			//Obtém uma sentença SQL.
			stmt = conn.createStatement();
			//Executa a instrução SQL.
			rs = stmt
					.executeQuery("select T.id_topic, T.Titulo, T.desc_topic, U.Nome as nome_usuario, T.data_criacao from TOPICO T, USUARIO U where U.ID_USER = T.ID_USER");
			//Percorre todos os registros.
			while (rs.next()) {
				int id_topico = rs.getInt("id_topic");
				String titulo = rs.getString("titulo");
				String nome_usuario = rs.getString("nome_usuario");
				String data = rs.getString("data_criacao");
				String mensagem = rs.getString("desc_topic");

				Topico topico = new Topico();
				topico.setId_topico(id_topico);
				topico.setData(data);
				topico.init_join(nome_usuario);
				topico.setMensagem(mensagem);
				topico.setTitulo(titulo);
				topicos.add(topico);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtil.close(conn, stmt, rs);
		}
		return topicos;
	}

	public void incluirTopico(Topico topico) {
		Connection conn = null;
		Statement stmt = null;
		try {
			conn = JdbcUtil.createConnection();
			//Inicia uma transação.
			conn.setAutoCommit(false);
			//Obtém uma sentença SQL.
			stmt = conn.createStatement();
			//Executa a instrução SQL.
//			String sql = "insert into topico (id_user, id_topic, titulo, data_criacao, Desc_Topic) values('1', '1', 'sodifj',null,'dsoifjdfi')";
//			stmt.executeUpdate(sql);
			
			PreparedStatement pstmt = conn.prepareStatement(
					"insert into topico "
					+ "(id_user, titulo, data_criacao, Desc_Topic) "
					+ "values "
					+ "(?, ?, ?, ?)");
			pstmt.setInt(1, UsuarioLogadoController.getInstance().getID_User());
			//pstmt.setInt(2, topico.geraChave_topico());
			pstmt.setString(2, topico.getTitulo());
			pstmt.setString(3, topico.agora());
			pstmt.setString(4, topico.getMensagem());
			pstmt.executeUpdate();

			//...
			//Isso é um crash!
			//if (topico.getId_topico().startsWith("1")) throw new RuntimeException("Yeh Yeh!");
			//...
			
//			PreparedStatement pstmt = conn.prepareStatement(
//					"insert into log_curso "
//					+ "(data_hora, operacao, codigo) "
//					+ "values "
//					+ "(?, ?, ?)");
//			pstmt.setTimestamp(1, agora());
//			pstmt.setString(2, "incluir");
//			pstmt.setString(3, topico.getId_topico());
//			pstmt.executeUpdate();

			//Efetivação da transação.
			conn.commit();
			
		} catch (Exception e) {

			try {
				if (conn != null && !conn.isClosed()) {
					//Cancela transação.
					conn.rollback();
				}
			} catch (Exception e1) {
				//Não faz nada.
			}

			throw new RuntimeException(e);
		} finally {
			JdbcUtil.close(conn, stmt);
		}
	}

//	private Timestamp agora() {
//		return new Timestamp(new Date().getTime());
//	}

	public void excluirTopico(Topico topico) {
		Connection conn = null;
		Statement stmt = null;
		try {
			conn = JdbcUtil.createConnection();
			//Obtém uma sentença SQL.
			stmt = conn.createStatement();
			//Executa a instrução SQL.
			stmt.executeUpdate("delete from topico where id_topic = "
					+ topico.getId_topico());
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			JdbcUtil.close(conn, stmt);
		}
	}
}
