package forum.Model;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Random;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import forum.Controller.UsuarioLogadoController;

@ManagedBean
@ApplicationScoped
public class Topico {	
	
	private String nome_usuario;
	
	private int id_usuario;
	private String data;
	private int id_topico;
	private String titulo;
	private String mensagem;

	public int getId_usuario() {
		return id_usuario;
		
	}

	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int getId_topico() {
		return id_topico;
	}
	
	public void setId_topico(int id_topico) {
		this.id_topico = id_topico;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	public String agora() {
		Timestamp dataTimestamp = new Timestamp(new Date().getTime());
		return dataTimestamp.toString();
	}

	public String getNome_usuario() {
		return nome_usuario;
	}

	public void init_join(String nome_usuario) {
		this.nome_usuario = nome_usuario;
	}

}
