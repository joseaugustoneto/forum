package forum.Controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import forum.Model.Topico;
import forum.Model.Usuario;
import forum.Model.UsuarioService;

@ManagedBean
@RequestScoped
public class CadastroUsuario {
	
	@ManagedProperty(value="#{usuarioService}")
	private UsuarioService usuarioService;
	
	private Usuario usuario = new Usuario();

	//Para fazer "cache" e ganhar desempenho.
	private List<Usuario> usuarios;

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public List<Usuario> getUsuarios() {
		//Implementação de "cache" da lista de usuarios.
		if (usuarios == null) {
			usuarios = usuarioService.obterUsuarios();
		}
		return usuarios;
	}
	
	public String salvar() {
		usuarioService.salvar(usuario);
		return "forum?faces-redirect=true";
	}
	
	public String excluir(Usuario topico) {
		usuarioService.excluir(topico);
		return "usuarioExcluido?faces-redirect=true";
	}
}