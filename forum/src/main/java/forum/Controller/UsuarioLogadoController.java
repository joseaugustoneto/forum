package forum.Controller;
import forum.Model.*;

import java.io.IOException;  

import javax.annotation.PostConstruct;  
import javax.faces.application.FacesMessage;  
import javax.faces.bean.ManagedBean;  
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;  
import javax.faces.context.FacesContext;  
  
@ManagedBean(name="UsuarioLogadoController")
@SessionScoped  
public class UsuarioLogadoController {  
     
	//@ManagedProperty(value="#{usuarioService}")
	private UsuarioService usuarioService = new UsuarioService();
    
	//@ManagedProperty(value="#{user}")
	private Usuario user = new Usuario();
	private Usuario userSession = new Usuario();
    private Boolean usuarioLogado = false;  
      
    private static UsuarioLogadoController instance;  
  
    @PostConstruct  
    public void inicializa()  
    {  
        //user = new Usuario();  
        usuarioLogado = Boolean.FALSE;  
        instance = this;  
    }  
    
    //Verifica se existe usuario logado na sessao
    public static UsuarioLogadoController getInstance() throws Exception  
    {  
        if(instance == null)  
        {  
            throw new Exception("Não há usuario logado no sistema.");  
        }  
        return instance;  
    }  
      //desloga o usuario.
    public void logout()  
    {  
        this.userSession = null;  
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();  
    }  
      
    public void fazerLogin()  
    {  
	    try {  

        	Usuario u = usuarioService.logar(user);  
            if(u == null)  
            {  
                FacesContext.getCurrentInstance().addMessage(null,   
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro teste", "Usuario não encontrado ou senha incorreta, tente novamente."));  
            }  
            else  
            {  
                usuarioLogado = true;  
                userSession = u;
                FacesContext.getCurrentInstance().addMessage(null,   
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Login Efetuado com sucesso!", "Erro ao efetuar login, tente novamente."));
            }  
              
        } catch (Exception e) {  
            FacesContext.getCurrentInstance().addMessage(null,   
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao efetuar login, tente novamente! Digite E-mail e Senha Validos", "Erro ao efetuar login, tente novamente."));  
        }
	    System.out.println(userSession.getCidade()+" usuariologado456");
    }  

	public String getNomeUsuario() //throws IOException  
    {  
		System.out.println(userSession.getCidade()+" usuariologado2");
		if(usuarioLogado)  
        {  
           return user.getEmail();  
        }else{    
        return "Login";  
    } }
     
    public Usuario getUser() {  
        return user;  
    }  
  
    public void setUser(Usuario user) {  
        this.user = user;  
    }
    
    public int getID_User(){
    	int i = userSession.getId_user();
    	return i;
    }
    
    public String getEmail(){
    	String i = userSession.getEmail();
    	return i;
    }
    
    public String getData(){
    	String i = userSession.getData_nascimento();
    	return i;
    }
    
    public String getNome(){
    	String i = userSession.getNome();
    	return i;
    }
    public String getCidade(){
    	String i = userSession.getCidade();
    	return i;
    }
    public String getUF(){
    	String i = userSession.getUf();
    	return i;
    }
      
}  