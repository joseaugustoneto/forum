package forum.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import forum.Model.*;
import forum.Controller.*;

public class CadastroServlet extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		

		
		try {

			
			HttpSession sessionUser = req.getSession();
			
			
			
			Usuarios user = new Usuarios();
			
			user.setNome(req.getParameter("nome"));
			user.setPassword(req.getParameter("senha"));
			user.setEmail(req.getParameter("email"));
			user.setDataNasc(req.getParameter("nascimento"));
			user.setSexo(req.getParameter("sexo"));
			user.setUf(req.getParameter("uf"));
			user.setCidade(req.getParameter("cidade"));
			
			
			if(user.getPassword().length()<5){
				PrintWriter imprime = resp.getWriter();
				imprime.println("<script>");
				imprime.println("alert('Senha muito pequena, a senha deve ser maior que cinco digitos!')");
				imprime.println("</script>");
				resp.getWriter().print("<a href=\"CadastrarUsuario.html\"><button onclick>Voltar> </a><br>");
			}else{
				sessionUser.setAttribute("Usuario", user);
				sessionUser.setAttribute("logado", 1);
				resp.sendRedirect ("UsuarioInformacoes.jsp");
				
				}
		
		} catch (Throwable e) {
			e.printStackTrace();
			resp.getOutputStream().print(
					"ERRO NESSA BAGA�A\"" + e.getMessage() + "\".");

		}

	}
}