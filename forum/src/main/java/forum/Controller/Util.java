package forum.Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Util {

	public static void dispatch(HttpServletRequest request, HttpServletResponse response, String path) throws ServletException, IOException {
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/" + path);

		requestDispatcher.forward(request, response);
	}

	public static Integer paramAsInteger(HttpServletRequest req, String param) {
		String paramValue = req.getParameter(param);
		paramValue = paramValue == null ? "0" : paramValue;

		return Integer.valueOf(paramValue);
	}

	public static Long paramAsLong(HttpServletRequest req, String param) {
		String paramValue = req.getParameter(param);
		paramValue = paramValue == null ? "0" : paramValue;

		return Long.valueOf(paramValue);
	}
}
