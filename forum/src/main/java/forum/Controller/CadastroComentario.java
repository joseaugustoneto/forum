package forum.Controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import forum.Model.ComentarioDao;
import forum.Model.Topico;
import forum.Model.Comentario;
import forum.Model.ComentarioService;

@ManagedBean
@RequestScoped
public class CadastroComentario {
	
	ComentarioService comentarioService = new ComentarioService();
	private Topico topico = new Topico();
	private Comentario comentario = new Comentario();
	public int cod_topico;

	//Para fazer "cache" e ganhar desempenho.
	private List<Comentario> comentarios;

	public void setUsuarioService(ComentarioService usuarioService) {
		this.comentarioService = usuarioService;
	}

	public Comentario getUsuario() {
		return comentario;
	}

	public void setUsuario(Comentario usuario) {
		this.comentario = usuario;
	}
	
	public List<Comentario> getComentarios() {
		
		setCod_topico(comentarioService.buscarCod());
		//Implementação de "cache" da lista de usuarios.
		if (comentarios == null) {
			comentarios = comentarioService.obterComentarios(getCod_topico());
		}
		return comentarios;
	}
	
	public String salvar() {
		comentarioService.salvar(comentario);
		return "usuarioSalvo?faces-redirect=true";
	}
	
	public String excluir(Comentario topico) {
		comentarioService.excluir(topico);
		return "usuarioExcluido?faces-redirect=true";
	}
	
	public String redireciona(Topico topico){
		this.topico = topico;
		//comentarioService.salvarCod(topico.getId_topico());
		return "Construcao?faces-redirect=true";
	}

	public int getCod_topico() {
		return cod_topico;
	}

	public void setCod_topico(int cod_topico) {
		this.cod_topico = cod_topico;
	}

}