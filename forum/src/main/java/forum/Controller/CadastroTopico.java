package forum.Controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import forum.Model.Topico;
import forum.Model.TopicoService;

@ManagedBean
@RequestScoped
public class CadastroTopico {
	
	@ManagedProperty(value="#{topicoService}")
	private TopicoService topicoService;
	
	private Topico topico = new Topico();

	//Para fazer "cache" e ganhar desempenho.
	private List<Topico> topicos;

	public void setTopicoService(TopicoService topicoService) {
		this.topicoService = topicoService;
	}

	public Topico getTopico() {
		return topico;
	}

	public void setTopico(Topico topico) {
		this.topico = topico;
	}
	
	public List<Topico> getTopicos() {
		//Implementação de "cache" da lista de topicos.
		if (topicos == null) {
			topicos = topicoService.obterTopicos();
		}
		return topicos;
	}
	
	public String salvar() {
		topicoService.salvar(topico);
		return "topicoSalvo?faces-redirect=true";
	}
	
	public String excluir(Topico topico) {
		topicoService.excluir(topico);
		return "topicoExcluido?faces-redirect=true";
	}
}