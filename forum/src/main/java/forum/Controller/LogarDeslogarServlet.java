package forum.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import forum.Model.Usuarios;

public class LogarDeslogarServlet extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		HttpSession sessionUser = req.getSession();
		
		Usuarios admin = new Usuarios();
		
		admin.setNome("Administrador");
		admin.setPassword("admin123");
		admin.setEmail("administrador");
		admin.setDataNasc("01/01/1500");
		admin.setSexo("M");
		admin.setUf("GO");
		admin.setCidade("Goi�nia");
		
		
		String acao;
		
		acao = req.getParameter("acao"); 
		
		if(acao.equals("Logar")){
			
			String nome, senha;
			
			nome = req.getParameter("login");
			senha = req.getParameter("senha");
			if(nome.equalsIgnoreCase(admin.getEmail()) && senha.equals(admin.getPassword())){
			
				sessionUser.setAttribute("Usuario", admin);
				sessionUser.setAttribute("logado", 1);
				resp.sendRedirect ("UsuarioInformacoes.jsp");
				
			}else{
				
				PrintWriter imprime = resp.getWriter();
				imprime.println("<script>");
				imprime.println("alert('Usuario N�o Cadastrado')");
				imprime.println("</script>");
				resp.sendRedirect ("index.jsp");
				
			}
		}
		else{
		
		sessionUser.invalidate();
		PrintWriter imprime = resp.getWriter();
		imprime.println("<script>");
		imprime.println("alert('Usuario deslogado')");
		imprime.println("</script>");
		resp.sendRedirect ("index.jsp");
		
		//resp.getWriter().print("<a href=\"index.jsp\"><button onclick>Home Page> </a><br>");
		
		}
	}
		
}
